const firstTemplate = `
<mjml>
  <mj-head>
    <mj-preview>10 tips to improve CTR&zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj;&zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj;&zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj; &zwnj;</mj-preview>
    <mj-breakpoint width="480px" />
    <mj-style>
      @media all and (max-width: 480px) { .dine-img { content: url("https://teamj-bucket.s3.amazonaws.com/business/business-dine.png"); width: 100%; } .comfort-img { content: url("https://teamj-bucket.s3.amazonaws.com/business/business-comfort.png"); width:
      100%; } .lounge-img { content: url("https://teamj-bucket.s3.amazonaws.com/business/business-lounge.png"); width: 100%; } .dine-text { background-color: "#000" }}
    </mj-style>
    <mj-title>Oman Air</mj-title>
    <mj-font name="Roboto" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500"></mj-font>
    <mj-attributes>
      <mj-all font-family="Montserrat, Helvetica, Arial, sans-serif"></mj-all>
      <mj-text font-weight="400" font-size="16px" color="#000000" line-height="24px"></mj-text>
      <mj-section padding="0px"></mj-section>
    </mj-attributes>
  </mj-head>
  <mj-body background-color="#E0E0E0" width="600px">
    <mj-section padding="20px"></mj-section>
    <mj-wrapper background-color="#fff">
      <mj-section background-color="#FFFFFF">
        <mj-column width="50%" vertical-align="middle">
          <mj-image align="left" src="https://upgrade-stage-cdn.plusgrade.com/offer/pid/6BP9Q6kvWY/email/logo_email.png" alt="oman air logo" align="center"></mj-image>
        </mj-column>
        <mj-column width="50%" vertical-align="middle">
          <mj-navbar base-url="https://mjml.io" hamburger="hamburger" ico-color="#00718F">
            <mj-navbar-link href="https://mjml.io" color="#00718F">عربى</mj-navbar-link>

            <mj-navbar-link href="https://mjml.io" color="#00718F">English</mj-navbar-link>
          </mj-navbar>

        </mj-column>
        <mj-column width="100%">
          <mj-image align="left" src="https://teamj-bucket.s3.amazonaws.com/pte-02/header-shadow.png" alt="oman air logo" padding-left=0 padding-right=0></mj-image>
        </mj-column>
      </mj-section>
      <mj-section>
        <mj-column>
          <mj-text align="center" color="#A8A8A8" font-size="11px" padding-top=0 padding-bottom=0>Booking reference: GC7J39</mj-text>
        </mj-column>
      </mj-section>
      <mj-section>
        <mj-column>
          <mj-text align="center" font-weight="600" font-size="30px" line-height="40px" padding-top=0 padding-bottom=0>You are eligible for Oman Air easy Upgrade</mj-text>
          <mj-text align="center" font-weight="100" font-size="15px" line-height="30px" padding-top=0 padding-bottom=0>Upgrade from economy to business in a few clicks</mj-text>
          <mj-button align="center" background-color="#00718F" font-weight="800" font-size="18px" color="#FFFFFF" border-radius="5px" href="#" inner-padding="15px 30px" padding-bottom="20px">Take me there</mj-button>
          <mj-divider border-color="#E0E0E0" border-width="1px" padding-left=0 padding-right=0></mj-divider>
        </mj-column>
      </mj-section>
      <mj-section>
        <mj-column>
          <mj-text align="center" font-weight="600" font-size="30px" line-height="40px" padding-bottom="10px">Why upgrade to business class?</mj-text>
        </mj-column>
      </mj-section>
      <mj-section background-color="#ffffff" full-width="full-width">
        <mj-column vertical-align="top" width="50%">
          <mj-image src="https://teamj-bucket.s3.amazonaws.com/business/business-dine.png" alt="" width="150px"></mj-image>
          <mj-text align="center" font-size="20px" padding-bottom=0 padding-top=0 line-height=2>Dine in style</mj-text>
        	<mj-text  align="center" font-size="15px" padding-bottom=0 padding-top=0 line-height=1.5>Savour our carefully selected gourmet meals.</mj-text>
        </mj-column>
        <mj-column vertical-align="top" width="50%">
          <mj-image src="https://teamj-bucket.s3.amazonaws.com/business/business-comfort.png" alt="" width="150px"></mj-image>
         <mj-text align="center" font-size="20px" padding-bottom=0 padding-top=0 line-height=2>Enjoy more comfort</mj-text><mj-text  align="center" font-size="15px" padding-bottom=0 padding-top=0 line-height=1.5>Relax in our award winning Business Class cabin.</mj-text>
        </mj-column>
      </mj-section>

      <mj-section>
        <mj-column>
          <mj-button align="center" background-color="#00718F" font-weight="800" font-size="18px" color="#FFFFFF" border-radius="5px" href="#" inner-padding="15px 30px" padding-bottom="20px" padding-top="40px">Get upgraded</mj-button>
          <mj-divider border-color="#E0E0E0" border-width="1px" padding-left=0 padding-right=0></mj-divider>
        </mj-column>
      </mj-section>
      <mj-section>
        <mj-column>
          <mj-text align="center" font-weight="600" font-size="30px" line-height="40px">Oman Air Easy Upgrade is available on the following flights</mj-text>
          <mj-image align="center" src="https://teamj-bucket.s3.amazonaws.com/schedule.png" alt="Man 1" width="500px"></mj-image>
          <mj-button align="center" background-color="#00718F" font-weight="800" font-size="18px" color="#FFFFFF" border-radius="5px" href="#" inner-padding="15px 30px" padding-bottom="20px">Get upgraded</mj-button>
          <mj-divider border-color="#E0E0E0" border-width="1px" padding-left=0 padding-right=0></mj-divider>
        </mj-column>
      </mj-section>
      <mj-section>
        <mj-column>
          <mj-text align="center" font-weight="600" font-size="30px" line-height="40px">Oman Air Easy Upgrade is as simple as 1-2-3</mj-text>
          <mj-text align="center" font-weight="300" font-size="25px" line-height="20px" color="#00718F">1. Place your bid</mj-text>
          <mj-text align="center" font-weight="100" font-size="15px">or place as many bids as you want</mj-text>
          <mj-image align="center" src="https://teamj-bucket.s3.amazonaws.com/gifs2/offerstrenght6.gif" alt="Man 1" padding-right="2px" padding-top="3px" width="230px" height="215px"></mj-image>
          <mj-text align="center" font-weight="300" font-size="25px" line-height="30px" color="#00718F">2. Submit your bid(s)</mj-text>
          <mj-text align="center" font-weight="100" font-size="15px">pay only if you win</mj-text>
          <mj-image align="center" src="https://teamj-bucket.s3.amazonaws.com/gifs2/submit.gif" alt="Man 1" padding-right="2px" padding-top="3px" width="114px" height="114px"></mj-image>
          <mj-text align="center" padding-bottom="20px" font-size="15px" font-weight="100">
            <p style="border-style: solid; border-color: #E0E0E0; border-width: thin; word-wrap: break-word;">You can modify or cancel your bid(s) up until 24 hours before each flight</p>
          </mj-text>
          <mj-text align="center" font-weight="300" font-size="30px" line-height="30px" color="#00718F">3. You fly upgraded!</mj-text>
          <mj-text align="center" font-weight="100" font-size="15px">Don't worry, if your bid is not accepted, nothing changes</mj-text>
          <mj-button align="center" background-color="#00718F" font-weight="800" font-size="18px" color="#FFFFFF" border-radius="5px" href="#" inner-padding="15px 30px" padding-bottom="20px">Get upgraded now</mj-button>
          <mj-image align="center" src="https://upgrade-cdn.plusgrade.com/offer/pid/6BP9Q6kvWY/CSE-BACKGROUND.jpg" alt="Man 1" padding-right=0 padding-top="3px" padding-left=0></mj-image>
        </mj-column>
      </mj-section>


      <mj-section padding="10px 0 20px 0">
        <mj-column>
          <mj-text align="center" color="#9B9B9B" font-size="11px"><a href="#" style="color: #9B9B9B;">Unsubscribe</a> from this newsletter<br/>xx Edison Court Suite xxx / Montreal / Canada<br/> <a href="#" style="color: #9B9B9B; text-decoration:none;">Made by teamJ</a></mj-text>
        </mj-column>
      </mj-section>
    </mj-wrapper>
    <mj-section padding="20px"></mj-section>
  </mj-body>
</mjml>
`;

const Templates = {
    firstTemplate
};

export default Templates;