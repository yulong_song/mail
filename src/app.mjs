import Koa from 'koa';
import Router from '@koa/router';
import TemplateGenerator from './TemplateGenerator.mjs';
import Email from "./Email.mjs";

const app = new Koa();
const router = new Router();
 
router.get('/generate', (ctx, next) => {
    const template = TemplateGenerator.generate().html.replace(/\n|\r|\\/g, "");
    ctx.body = {template: Buffer.from(template).toString("base64")};
  });

router.get("/send/:email", async (ctx) => {
  try {
    await Email.sendEmail(ctx.params.email, TemplateGenerator.generate().html.replace(/\n|\r|\\/g, ""));
  }catch(e) {
    console.error(e);
  }
    
})
  
// response
app
  .use(router.routes())
  .use(router.allowedMethods());

console.log("server is running on localhost:3000");
app.listen(3000);