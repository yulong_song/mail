import Nodemailer from "nodemailer";


const sendEmail = async (email, content) => {
    let transporter = Nodemailer.createTransport({
        host: 'smtp.googlemail.com', // Gmail Host
        port: 465, // Port
        secure: true, // this is true as port is 465
        auth: {
            user: 'plusgrade.test123@gmail.com', // generated ethereal user
            pass: 'PlusgradeTest123', // generated ethereal password
        },
    });
 
    // send mail with defined transport object
    return transporter.sendMail({
        from: '"TeamJ" <plusgrade.test123@gmail.com>', // sender address
        to: email, // list of receivers
        subject: "Badass PTE", // Subject line
        html: content, // html body
    });
}

const Email = {
    sendEmail
};

export default Email;