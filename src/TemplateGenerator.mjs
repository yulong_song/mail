import mjml2html from 'mjml'
import Templates from "./Templates.mjs";


const generate = () => {
    return mjml2html(Templates.firstTemplate, {minify: true})
}

const TemplateGenerator = {
  generate
}

export default TemplateGenerator;
